<?php

namespace Competi\Adapters;

interface SendMailAdapter
{
    public function api(object $email): bool;
    public function sdk(object $email): bool;
    public function smtp(object $email): bool;
}