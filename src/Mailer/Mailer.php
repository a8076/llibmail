<?php

namespace Competi\Mailer;

use Competi\Adapters\SendMailAdapter;

class Mailer implements MailerTemplate
{
    private SendMailAdapter $sendMailAdapter;
    protected object $dataForMail;

    public function __construct(SendMailAdapter $sendMailAdapter, object $dataForMail)
    {
        $this->sendMailAdapter = $sendMailAdapter;
        $this->dataForMail = $dataForMail;
    }

    public function api(): bool
    {
        return $this->sendMailAdapter->api($this->dataForMail);
    }

    public function smtp(): bool
    {
        return $this->sendMailAdapter->smtp($this->dataForMail);
    }

    public function sdk(): bool
    {
        return $this->sendMailAdapter->sdk($this->dataForMail);
    }
}