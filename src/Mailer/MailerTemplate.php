<?php

namespace Competi\Mailer;

use Competi\Adapters\SendMailAdapter;

interface MailerTemplate
{
    public function __construct(SendMailAdapter $sendMailAdapter, object $dataForMail);
    public function api(): bool;
    public function smtp(): bool;
    public function sdk(): bool;
}