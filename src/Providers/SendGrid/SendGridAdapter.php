<?php

namespace Competi\Providers\SendGrid;

use Competi\Providers\BaseAdapter;

class SendGridAdapter extends BaseAdapter
{
    public function __construct()
    {
        $this->usernameSmtp = config('app.username_smtp');
        $this->passwordSmtp = config('app.password_smtp');
        $this->host = config('app.host_smtp') ?? 'smtp.sendgrid.net';
        $this->port = config('app.host_port') ?? 587;
    }
}