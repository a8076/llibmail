<?php

namespace Competi\Providers\Google;

use Competi\Providers\BaseAdapter;
use PHPMailer\PHPMailer\PHPMailer;
use stdClass;

class GoogleAdapter extends BaseAdapter
{
    public function __construct()
    {
        $this->usernameSmtp = config('app.username_smtp');
        $this->passwordSmtp = config('app.password_smtp');
        $this->host = config('app.host') ?? 'smtp.gmail.com';
    }

    protected function getProps($email)
    {
        $this->remetente = new stdClass;
        $this->remetente->nome = $email->remetente->nome ?? null;
        $this->remetente->endereco = self::required($this->usernameSmtp, 'endereço do remetente');
        $this->destinatario = self::required($email->destinatario, 'endereço do destinatário');
        $this->assunto = self::required($email->assunto, 'assunto');
        $this->msgHtml = $email->msgHtml ?? null;
        $this->msgText = $email->msgText ?? null;
        $this->SMTPDebug = $email->SMTPDebug ?? null;

        return $this;
    }

    protected function setConfig()
    {
        $this->mail = new PHPMailer(true);

        // Specify the SMTP settings.
        $this->mail->isSMTP();
        $this->mail->Username   = $this->usernameSmtp;
        $this->mail->Password   = $this->passwordSmtp;
        $this->mail->Host       = $this->host;
        $this->mail->Port       = $this->port;
        $this->mail->SMTPAuth   = true;
        $this->mail->SMTPSecure = 'tls';
        $this->mail->SMTPDebug = $this->SMTPDebug;
        return $this;
    }
}